package br.com.ozcorp;
/**
 * Classe que cria analistas na empresa
 * @author Marcos Sim�es Bernardo
 *
 */
public class Analista extends Funcionario{

	public Analista(String nome, String rg, String cpf, String matricula, String email, String senha, Cargo cargo,
			Departamento departamento, int nivelAcesso, Sexo sexo, TipoSanguineo tipoSanguineo) {
		super(nome, rg, cpf, matricula, email, senha, cargo, departamento, nivelAcesso, sexo, tipoSanguineo);
	}
	
}
