package br.com.ozcorp;
/**
 * Classe que cria cargos na empresa
 * @author Marcos Sim�es Bernardo
 *
 */
public class Cargo {
	
	//Atributos
	private String titulo;
	private double salarioBase;
	
	// Construtor
	public Cargo(String titulo, double salarioBase) {
		this.titulo = titulo;
		this.salarioBase = salarioBase;
	}
	
	/*
	 *  Getters e Setters
	 */
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}
}
