package br.com.ozcorp;
/**
 * Classe que cria departamentos na empresa
 * @author Marcos Sim�es Bernardo
 *
 */
public class Departamento {
	
	// Atributos
	private String nome;
	private String sigla;
	private Cargo cargo;
	
	// Construtor
	public Departamento(String nome, String sigla, Cargo cargo) {
		this.nome = nome;
		this.sigla = sigla;
		this.cargo = cargo;
	}
	
	/*
	 * Getters e Setters
	 */
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	
}
