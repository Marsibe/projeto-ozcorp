package br.com.ozcorp;
/**
 * Classe que cria funcionarios na empresa
 * @author Marcos Sim�es Bernardo
 *
 */
public class Funcionario {
	
	// Atributos
	private String nome;
	private String rg;
	private String cpf;
	private String matricula;
	private String email;
	private String senha;
	private Cargo cargo;
	private Departamento departamento;
	private int nivelAcesso;
	private Sexo sexo;
	private TipoSanguineo tipoSanguineo;
	
	// Construtor
	public Funcionario(String nome, String rg, String cpf, String matricula, String email, String senha, Cargo cargo,
			Departamento departamento, int nivelAcesso, Sexo sexo, TipoSanguineo tipoSanguineo) {
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		this.email = email;
		this.senha = senha;
		this.cargo = cargo;
		this.departamento = departamento;
		this.nivelAcesso = nivelAcesso;
		this.sexo = sexo;
		this.tipoSanguineo = tipoSanguineo;
	}
	
	/*
	 * Getters e Setters
	 */
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public int getNivelAcesso() {
		return nivelAcesso;
	}

	public void setNivelAcesso(int nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public TipoSanguineo getTipoSanguineo() {
		return tipoSanguineo;
	}

	public void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}
}
