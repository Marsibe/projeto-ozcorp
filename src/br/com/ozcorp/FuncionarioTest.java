package br.com.ozcorp;
/**
 * Classe que instancia funcionarios na empresa
 * @author Marcos Sim�es Bernardo
 *
 */
public class FuncionarioTest {

	public static void main(String[] args) {

		Funcionario zoio = new Funcionario("Marcos", "266876122", "17682202807", "0123456", 
				"simoes.marcos@hotmail.com", "29041974", 
				new Cargo("Analista", 2500.00), new Departamento("Recursos Humanos", "RH", 
						new Cargo("Analista", 2500.00)), 4, Sexo.MASCULINO, TipoSanguineo.A_POSITIVO);
		
		System.out.println("DADOS DO FUNCION�RIO: ");
		System.out.println("Nome:                      " + zoio.getNome());
		System.out.println("Rg:                        " + zoio.getRg());
		System.out.println("CPF:                       " + zoio.getCpf());
		System.out.println("Matricula:                 " + zoio.getMatricula());
		System.out.println("E-mail:                    " + zoio.getEmail());
		System.out.println("Senha:                     " + zoio.getSenha());
		System.out.println("Cargo:                     " + zoio.getCargo().getTitulo() + " " + zoio.getDepartamento().getNome());
		System.out.println("Salario:                   " + zoio.getCargo().getSalarioBase());
		System.out.println("Tipo sanguineo:            " + zoio.getTipoSanguineo().nome);
		System.out.println("Sexo:                      " + zoio.getSexo().nome);
		System.out.println("Nivel de acesso:           " + zoio.getNivelAcesso());
		
		System.out.println();
		
		Funcionario goiaba = new Funcionario("Rafael", "22233344402", "11122233300", "1062435", 
				"rafa@hotmail.com", "22233314", 
				new Cargo("Diretor", 12000.00), new Departamento("Financeiro", "FIN", 
						new Cargo("Diretor", 12000.00)), 0, Sexo.OUTRO, TipoSanguineo.O_NEGATIVO);
		
		System.out.println("DADOS DO FUNCION�RIO: ");
		System.out.println("Nome:                      " + goiaba.getNome());
		System.out.println("Rg:                        " + goiaba.getRg());
		System.out.println("CPF:                       " + goiaba.getCpf());
		System.out.println("Matricula:                 " + goiaba.getMatricula());
		System.out.println("E-mail:                    " + goiaba.getEmail());
		System.out.println("Senha:                     " + goiaba.getSenha());
		System.out.println("Cargo:                     " + goiaba.getCargo().getTitulo() + " " + goiaba.getDepartamento().getNome());
		System.out.println("Salario:                   " + goiaba.getCargo().getSalarioBase());
		System.out.println("Tipo sanguineo:            " + goiaba.getTipoSanguineo().nome);
		System.out.println("Sexo:                      " + goiaba.getSexo().nome);
		System.out.println("Nivel de acesso:           " + goiaba.getNivelAcesso());
		
		System.out.println();
		
		Funcionario xampinha = new Funcionario("Vitor", "0123456789", "44455566677", "548963", 
				"vitao@hotmail.com", "11155589", 
				new Cargo("Gerente", 7000.00), new Departamento("Comercial", "COM", 
						new Cargo("Gerente", 7000.00)), 2, Sexo.MASCULINO, TipoSanguineo.AB_POSITIVO);
		
		System.out.println("DADOS DO FUNCION�RIO: ");
		System.out.println("Nome:                      " + xampinha.getNome());
		System.out.println("Rg:                        " + xampinha.getRg());
		System.out.println("CPF:                       " + xampinha.getCpf());
		System.out.println("Matricula:                 " + xampinha.getMatricula());
		System.out.println("E-mail:                    " + xampinha.getEmail());
		System.out.println("Senha:                     " + xampinha.getSenha());
		System.out.println("Cargo:                     " + xampinha.getCargo().getTitulo() + " " + xampinha.getDepartamento().getNome());
		System.out.println("Salario:                   " + xampinha.getCargo().getSalarioBase());
		System.out.println("Tipo sanguineo:            " + xampinha.getTipoSanguineo().nome);
		System.out.println("Sexo:                      " + xampinha.getSexo().nome);
		System.out.println("Nivel de acesso:           " + xampinha.getNivelAcesso());
		
		System.out.println();
		
		Funcionario xaropinha = new Funcionario("Bianca", "4781562304", "66655522289", "123456", 
				"bianca@hotmail.com", "22255569", 
				new Cargo("Engenheira", 5000.00), new Departamento("Manuten��o", "MAN", 
						new Cargo("Engenheira", 5000.00)), 3, Sexo.FEMININO, TipoSanguineo.B_NEGATIVO);
		
		System.out.println("DADOS DO FUNCION�RIO: ");
		System.out.println("Nome:                      " + xaropinha.getNome());
		System.out.println("Rg:                        " + xaropinha.getRg());
		System.out.println("CPF:                       " + xaropinha.getCpf());
		System.out.println("Matricula:                 " + xaropinha.getMatricula());
		System.out.println("E-mail:                    " + xaropinha.getEmail());
		System.out.println("Senha:                     " + xaropinha.getSenha());
		System.out.println("Cargo:                     " + xaropinha.getCargo().getTitulo() + " " + xaropinha.getDepartamento().getNome());
		System.out.println("Salario:                   " + xaropinha.getCargo().getSalarioBase());
		System.out.println("Tipo sanguineo:            " + xaropinha.getTipoSanguineo().nome);
		System.out.println("Sexo:                      " + xaropinha.getSexo().nome);
		System.out.println("Nivel de acesso:           " + xaropinha.getNivelAcesso());
		
		System.out.println();
		
		Funcionario bozolina = new Funcionario("Rayssa", "4781562304", "66655522289", "123456", 
				"rayssa@hotmail.com", "22255569", 
				new Cargo("Secretaria", 3000.00), new Departamento("Expedi��o", "EXP", 
						new Cargo("Secretaria", 3000.00)), 1, Sexo.FEMININO, TipoSanguineo.B_POSITIVO);
		
		System.out.println("DADOS DO FUNCION�RIO: ");
		System.out.println("Nome:                      " + bozolina.getNome());
		System.out.println("Rg:                        " + bozolina.getRg());
		System.out.println("CPF:                       " + bozolina.getCpf());
		System.out.println("Matricula:                 " + bozolina.getMatricula());
		System.out.println("E-mail:                    " + bozolina.getEmail());
		System.out.println("Senha:                     " + bozolina.getSenha());
		System.out.println("Cargo:                     " + bozolina.getCargo().getTitulo() + " " + bozolina.getDepartamento().getNome());
		System.out.println("Salario:                   " + bozolina.getCargo().getSalarioBase());
		System.out.println("Tipo sanguineo:            " + bozolina.getTipoSanguineo().nome);
		System.out.println("Sexo:                      " + bozolina.getSexo().nome);
		System.out.println("Nivel de acesso:           " + bozolina.getNivelAcesso());
	}

}
