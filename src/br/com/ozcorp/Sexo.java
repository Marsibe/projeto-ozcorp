package br.com.ozcorp;
/**
 * Enumera��o que lista sexos na empresa
 * @author Marcos Sim�es Bernardo
 *
 */
public enum Sexo {
	
MASCULINO("Masculino"), FEMININO("Feminino"), OUTRO("Outro");
	
	public String nome;
	
	Sexo(String nome) {
		this.nome = nome;
	}
}
