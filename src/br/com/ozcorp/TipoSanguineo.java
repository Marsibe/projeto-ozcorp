package br.com.ozcorp;
/**
 * Enumera��o que lista tipos sanguineos na empresa
 * @author Marcos Sim�es Bernardo
 *
 */
public enum TipoSanguineo {
	A_POSITIVO("A+"), A_NEGATIVO("A-"), B_POSITIVO("B+"), B_NEGATIVO("B-"), O_POSITIVO("O+"), O_NEGATIVO("O-"), AB_POSITIVO("AB+"), AB_NEGATIVO("AB-");
	
	public String nome;
	
	TipoSanguineo(String nome) {
		this.nome = nome;
	}	
}
